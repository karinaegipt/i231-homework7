
import java.util.*;

/**
 * Prefix codes and Huffman tree.
 * Tree depends on source data.
 */
public class Huffman {

   private HuffmanMap huffmanMap;
   private LetterLeaf tree;
   private int length;

   /**
    * Separate class to generate map containing character and byte value
    */
   public class HuffmanMap extends HashMap<Character, String> {

      /**Method to return characters code
       * @param chr
       * @return String assigned byte value
        */
      public String findCode(Character chr){
         try {
            return get(chr);
         } catch (RuntimeException e) {
            throw new RuntimeException("There is no such character: " + chr);
         }
      }
   }

   /**Method to create map with Character and frequency value
    * @param chars character array in byte[]
    * @return Map<Character, Integer>
    */
   public Map<Character, Integer> getFrequency (byte[] chars) {
      Map<Character, Integer> frequencyMap = new HashMap<Character, Integer>();
        for (int i = 0; i < chars.length; i++) {
           Character c = (char)chars[i];
           if (!frequencyMap.containsKey(c)) {
              frequencyMap.put(c, 1);
           } else {
              int frequency = frequencyMap.get(c);
              frequency++;
              frequencyMap.put(c, frequency);
           }
        }
        return frequencyMap;
   }

   /***
    * Method to convert Map to list (sorted)
    * @param frequency Map
    * @return list of map values
     */
   public List<LetterLeaf> convertFreqToList(Map frequency) {
      List<LetterLeaf> letters = new ArrayList<LetterLeaf>();
      Iterator<Map.Entry<Character, Integer>> iterator = frequency.entrySet().iterator();
      while (iterator.hasNext()){
         final Map.Entry<Character, Integer> entry = iterator.next();
         letters.add(new LetterLeaf(){{
            this.chr = entry.getKey();
            this.freq = entry.getValue();
         }});
      }
      sortLettersList(letters);
      return letters;
   }

   /**
    * Method for sorting list
    * @param list of sorted values
     */
   public void sortLettersList(List<LetterLeaf> list) {
      Collections.sort(list, new Comparator<LetterLeaf>() {
         @Override
         public int compare(LetterLeaf o1, LetterLeaf o2) {
            return o1.freq < o2.freq ? -1 : o1.freq == o2.freq ? 0 : 1;
         }
      });
   }

   /** Constructor for creating leaf nodes*/
   public class LetterLeaf {
      Character chr;
      Integer freq;
      LetterLeaf childRight = null;
      LetterLeaf childLeft = null;
   }

   /**
    * Method for building Huffman tree
    * @param letterLeafs sorted list of leafs
    * @return vertex
     */
   public LetterLeaf buildTree(List<LetterLeaf> letterLeafs){
      while (letterLeafs.size() > 1){
         final LetterLeaf letterLeaf1 = letterLeafs.remove(0);
         final LetterLeaf letterLeaf2 = letterLeafs.remove(0);
         LetterLeaf combinedLeaf = new LetterLeaf(){{
            this.chr = null;
            this.freq = letterLeaf1.freq + letterLeaf2.freq;
            this.childLeft = letterLeaf1;
            this.childRight = letterLeaf2;
         }};
         letterLeafs.add(combinedLeaf);
         sortLettersList(letterLeafs);
      }
      return letterLeafs.get(0);
   }

   /**
    * Method to create map with Characters and corresponding binary codes
    * @param letterLeaf node
    * @return map of characters and corresponding binary codes
     */
   public HuffmanMap getCodesMap(LetterLeaf letterLeaf) {
      HuffmanMap letterLeafMap = new HuffmanMap();
      getCode(letterLeaf, letterLeafMap, "");
      return letterLeafMap;
   }

   /**
    * Method to get code for current node using given HuffmanMap
    * @param leaf node
    * @param letterLeafMap HuffmanMap with Characters and codes
    * @param coding string value to store the code
     */
   public void getCode(LetterLeaf leaf, HuffmanMap letterLeafMap, String coding){
      if (leaf.chr != null){
         if (coding.isEmpty()) {
            coding += '0';
         }
         letterLeafMap.put(leaf.chr, coding);
      } else {
         getCode(leaf.childLeft, letterLeafMap, coding + "0");
         getCode(leaf.childRight, letterLeafMap, coding + "1");
      }
   }


   /** Constructor to build the Huffman code for a given bytearray.
    * @param original source data
    */
   Huffman (byte[] original) {
      Map huffman = getFrequency(original);
      List<LetterLeaf> x = convertFreqToList(huffman);
      tree = buildTree(x);
      huffmanMap = getCodesMap(tree);
   }

   /** Length of encoded data in bits. 
    * @return number of bits
    */
   public int bitLength() {
      return length;
   }


   /** Encoding the byte array using this prefixcode.
    * @param origData original data
    * @return encoded data
    */
   public byte[] encode (byte [] origData) {
      String output = "";
      for (int i = 0; i < origData.length; i++) {
         output += huffmanMap.findCode((char)origData[i]);
      }
      length = output.length();
      int l = output.length() % 8;
      if (l > 0) {
         output += new String(new char[8 - l]).replace("\0", "0");
      }

      byte[] encoded = new byte[output.length() / 8 + 1];
      encoded[0] = (byte)(8 - l);

      for(int i = 0; i <= output.length() - 8; i += 8) {
         encoded[(i + 8) / 8] = (byte)(Integer.parseInt(output.substring(i, i + 8), 2) & 0xFF);
      }
      return encoded;
   }

   /** Decoding the byte array using this prefixcode.
    * @param encodedData encoded data
    * @return decoded data (hopefully identical to original)
    */
   public byte[] decode (byte[] encodedData) {
      String input = "";
      int padding = encodedData[0] & 0xFF;
      for (int i = 1; i < encodedData.length; i++) {
         input += Integer.toBinaryString((encodedData[i] & 0xFF) + 0x100).substring(1);
      }

      input = input.substring(0, input.length() - padding);

      String output = "";
      LetterLeaf tree = this.tree;
      for(Character chr : input.toCharArray()) {
         if (chr == '0') {
            if (tree.childLeft != null) {
               tree = tree.childLeft;
            }
         } else {
            tree = tree.childRight;
         }
         if (tree.chr != null) {
            output += tree.chr;
            tree = this.tree;
         }
      }
      return output.getBytes();
   }

   /** Main method. */
   public static void main (String[] params) {
      String tekst = "AAAAAAAAAAAAABBBBBBCCCDDEEF";
      byte[] orig = tekst.getBytes();
      Huffman huf = new Huffman (orig);
      byte[] kood = huf.encode (orig);
      byte[] orig2 = huf.decode (kood);
      // must be equal: orig, orig2
      System.out.println (Arrays.equals (orig, orig2));
      int lngth = huf.bitLength();
      System.out.println ("Length of encoded data in bits: " + lngth);

   }
}

